## English

### Waterfall Chart

This project is a Three.js-based waterfall chart visualization.

![Waterfall Chart](https://gitee.com/daxiangchaorou/dxcr-waterfall/raw/master/shot/shot1.png)

#### Features

- Uses Three.js for rendering.
- WebWorker for background data processing.
- Dynamic data update and chart scaling.
- Interactive chart with mouse events.
- Axis with ticks and labels.
  
## Usage Instructions
```javascript
import Waterfall from 'dxcr-waterfall';
// Create a Waterfall instance
const waterfall = new Waterfall(dom, {
    debugMode: false, // Debug mode
    dataCount: 1000, // Number of data per row
    keepCount: 1000, // Total number of rows
    minValue: 0, // Minimum value for data-generated colors
    maxValue: 3000, // Maximum value for data-generated colors
});
```

#### Code Structure

**Waterfall class**: Handles the main chart functionalities.
- `addData(data, index)`: Adds new data.
- `setData(data)`: Sets the data.
- `clearData()`: Clears the data.

## 中文

### 瀑布图

该项目是一个基于 Three.js 的瀑布图可视化工具。

![瀑布图](https://gitee.com/daxiangchaorou/dxcr-waterfall/raw/master/shot/shot1.png)

#### 特性

- 使用 Three.js 进行渲染。
- 使用 WebWorker 进行后台数据处理。
- 动态数据更新和图表缩放。
- 支持鼠标事件的交互式图表。
- 带有刻度和标签的轴。

## 使用说明
```javascript
import Waterfall from 'dxcr-waterfall'
// 创建 Waterfall 实例
const waterfall = new Waterfall(dom, {
    debugMode: false,//debug模式
    dataCount: 1000,//每行数据量
    keepCount: 1000,//总行数
    minValue: 0,//数据生成的颜色最小值
    maxValue: 3000,//数据生成的颜色最大值
});
```

#### 代码结构
**Waterfall 类**: 处理主要的图表功能。
- `addData(data, index)`: 添加数据。
- `setData(data)`: 设置图表数据。
- `clearData()`: 清除图表数据。
