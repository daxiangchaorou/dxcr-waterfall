/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2024-05-17 16:49:34
 * @LastEditTime: 2024-07-08 16:31:41
 * @LastEditors: Dxcr
 */
let f = false;
self.addEventListener("message", (e) => {
  if (!f) {
    console.log("worker", 1);
    f = true;
  }
  let eData = e.data;
  let type = eData.type;
  let returnObject = {};
  if (type == 1) {
    //批量处理
    let { data, minValue, maxValue, dataCount } = eData;
    returnObject.data = convertDatas(data, minValue, maxValue, dataCount);
    returnObject.type = 1;
  } else {
    //批量处理
    let { data, minValue, maxValue, dataCount } = eData;
    returnObject.data = convertData(data, minValue, maxValue, dataCount);
    returnObject.type = 0;
    returnObject.index = eData.index;
  }

  self.postMessage(returnObject); // 将接收到的数据直接返回
});
const colors1 = ["#f5222d", "#fa541c", "#fa8c16", "#faad14", "#fadb14"];
// const colors = [
//   [0.9804, 0.8588, 0.0784],
//   [0.9804, 0.6784, 0.0784],
//   [0.9804, 0.549, 0.0863],
//   [0.9804, 0.3294, 0.1098],
//   [0.9608, 0.1333, 0.1765],
// ];
const colors = [
  [0, 0, 1],
  [0.25, 0.5882, 1],
  [0.2118, 0.8118, 0.7882],
  [0.451, 0.8196, 0.2392],
  [0.7294, 0.902, 0.2157],
  [1, 0.9255, 0.2392],
  [1, 1, 0],
];
function convertData(datas, minValue, maxValue, dataCount) {
  let count = datas.length;
  dataCount = dataCount ?? count;

  let dataColors = new Float32Array(dataCount * 3);
  let sampledDatas = sampleData(datas, count / dataCount);

  for (let i = 0; i < sampledDatas.length; i++) {
    let value = sampledDatas[i];
    let colors = getGradientColor(value, minValue, maxValue);
    dataColors[i * 3] = colors[0];
    dataColors[i * 3 + 1] = colors[1];
    dataColors[i * 3 + 2] = colors[2];
  }

  return dataColors;
}

function convertDatas(datas, minValue, maxValue, dataCount) {
  let results = [];

  for (let i = 0; i < datas.length; i++) {
    let result = convertData(datas[i], minValue, maxValue, dataCount);
    results.push(result);
  }

  return results;
}

function getGradientColor(value, minValue, maxValue) {
  const range = maxValue - minValue; // 总范围从固定值改为动态计算
  const part = range / (colors.length - 1); // 每个颜色过渡的范围

  // 确定value位于哪个颜色过渡范围内
  let index = Math.min(
    Math.floor((value - minValue) / part),
    colors.length - 2
  );
  index = Math.max(index, 0);
  if(isNaN(index)){
    index = 0
  }

  let fraction = ((value - minValue) % part) / part; // 当前范围内的比例
  if(isNaN(fraction)){
    fraction = 0
  }

  // 计算渐变色
  let color1 = colors[index];
  let color2 = colors[index + 1];
  let gradientColor = interpolateColors(color1,color2,fraction)
  // let gradientColor = color1.map((c, i) => c + (color2[i] - c) * fraction);

  return gradientColor;
}

function interpolateColors(color1, color2, fraction) {
  let interpolatedColor = [];

  // 计算插值
  for (let i = 0; i < color1.length; i++) {
    let interpolatedValue = color1[i] + (color2[i] - color1[i]) * fraction;
    interpolatedColor.push(interpolatedValue);
  }

  return interpolatedColor;
}

function sampleData(datas, sampleInterval) {
  let sampledData = [];
  let sampleSize = datas.length;

  for (let i = 0; i < sampleSize; i += sampleInterval) {
    let sampleSegment = datas.slice(i, Math.ceil(i + sampleInterval));
    if (sampleSegment.length > 0) {
      let maxInSegment = findMax(sampleSegment);
      sampledData.push(maxInSegment);
    } else {
      sampledData.push(sampledData[sampledData.length - 1]);
    }
  }

  return sampledData;
}

function findMax(sampleSegment) {
  let maxInSegment = sampleSegment[0]; // 初始化最大值为数组的第一个元素
  for (let i = 1; i < sampleSegment.length; i++) {
    if (sampleSegment[i] > maxInSegment) {
      maxInSegment = sampleSegment[i]; // 更新最大值
    }
  }
  return maxInSegment; // 返回最大值
}

export default self;
