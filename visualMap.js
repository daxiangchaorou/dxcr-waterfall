/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2024-07-05 10:03:58
 * @LastEditTime: 2024-07-08 10:06:45
 * @LastEditors: Dxcr
 */
export default class VisualMap {
  constructor(dom) {
    this.dom = dom;
    this.minLabel = null;
    this.maxLabel = null;
    this.createVisualMap();
  }

  createVisualMap() {
    // 创建 visualMap 的 DOM 结构
    this.visualMapDom = document.createElement("div");
    this.visualMapDom.className = "visual-map";
    this.visualMapDom.style.width = "20px";
    this.visualMapDom.style.height = "100px";
    this.visualMapDom.style.display = "flex";
    this.visualMapDom.style.alignItems = "center";
    this.visualMapDom.style.justifyContent = "center";
    this.visualMapDom.style.background =
      "linear-gradient(to top, #ff0000, #0000ff)";
    this.dom.appendChild(this.visualMapDom);

    // 创建最小值和最大值标签
    this.minLabel = document.createElement("div");
    this.minLabel.className = "label min";
    this.minLabel.style.position = "absolute";
    this.minLabel.style.bottom = "-16px";
    this.minLabel.style.color = "#fff";
    this.minLabel.textContent = "0";
    this.visualMapDom.appendChild(this.minLabel);

    this.maxLabel = document.createElement("div");
    this.maxLabel.className = "label max";
    this.maxLabel.style.position = "absolute";
    this.maxLabel.style.top = "-16px";
    this.maxLabel.style.color = "#fff";
    this.maxLabel.textContent = "100";
    this.visualMapDom.appendChild(this.maxLabel);
  }

  bindLabelDoubleClick(fuc) {
    // 双击事件处理程序
    const doubleClickHandler = (event, type) => {
      const label = event.target;
      const currentValue = label.textContent;

      // 创建输入框
      const input = document.createElement("input");
      input.type = "text";
      input.value = currentValue;
      input.style.position = "absolute";
      input.style.textAlign = "center";
      input.style.fontSize = "12px";
      input.style.color = "#fff"; // 输入框文字颜色
      input.style.background = "#000"; // 输入框背景颜色
      input.style.width = label.offsetWidth + 12 + "px";
      input.style.height = label.offsetHeight + 2 + "px";
      input.style.top = label.offsetTop + "px";
      input.style.padding = "0px";

      // 替换标签为输入框
      this.visualMapDom.replaceChild(input, label);

      // 监听输入框失去焦点事件
      input.addEventListener("blur", () => {
        const newValue = input.value.trim();
        // 如果输入框内容不为空，则更新标签内容
        if (newValue !== "") {
          label.textContent = newValue;
        }
        // 将输入框替换回标签
        this.visualMapDom.replaceChild(label, input);

        fuc(type, newValue);
      });

      // 让输入框获得焦点
      input.focus();
    };

    // 绑定双击事件到标签
    this.minLabel.addEventListener("dblclick", (event) =>
      doubleClickHandler(event, 0)
    );
    this.maxLabel.addEventListener("dblclick", (event) =>
      doubleClickHandler(event, 1)
    );
  }

  updateVisualMap(colors, minLabel, maxLabel) {
    // 创建颜色渐变字符串
    const gradient = colors.join(", ");
    // 更新 visualMap 的颜色渐变
    this.visualMapDom.style.background = `linear-gradient(to top, ${gradient})`;

    // 更新标签文本内容
    this.minLabel.textContent = minLabel;
    this.maxLabel.textContent = maxLabel;
  }
}
